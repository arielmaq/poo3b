<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos.css">
	<title>Document</title>

</head>
<body>
	<section class="container">
		<div class="row">
			<div class="col-sm-12"></div>
			<div class="col-sm-12">
				<div class="my-5"></div>
				<form action="checklogin.php" class="login" method="POST">
					<?php
						error_reporting(E_ALL ^ E_NOTICE);
						if ($GET["error"=="si"]) {
							echo '<div class="alert alert-danger" role="alert"><center><strong>Ops! verifica tus datos</strong></center> </div>';
							# code...
						}
						else{echo "";}
					?>
					<h2><center>USUARIO</center></h2>
					<div><img src="img/avatar.png" alt="" id="avatar"></div>
					<input type="text" class="form-control" placeholder="Introduzca usuario" name="user" id="user">
					<input type="password" class="form-control" placeholder="Introduzca contraseña" name="pass" id="pass">
					<div>
						<label for="" class="custom-control custom-checkbox">
							<input type="checkbox" class="bg-primary custom-control-input">
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description text-dark">Recordar contraseña en esta PC</span>
						</label>
					</div>
					<button type="submit" name="submit" id="submit" class="btn btn-primary btn-lg btn-block">Iniciar Sesion</button>
					<span><a href="index.php" class="btn btn-danger btn-lg btn-block">Limpiar Datos</a></span>	
				</form>
			</div>
		</div>
	</section>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/popper.min.js"></script>
</body>
</html>