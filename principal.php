<?php
include 'session.php';
include 'lib/config.php';
include 'lib/Mdb.php';
include 'inc/header.php';
?>
<div class="col-sm-12">
	<?php 
	$db=new ManejadorDB();
	$query="SELECT *FROM tbl_usern";
	$read=$db->select($query);	
	
	?>
	<?php
		if (isset($_GET['msg'])) {
			echo "<div class='alert alert-success'><span>".$_GET['msg']."</span></div>";
		}
	?>	
</div>
<div class="col-sm-12">
<table class="table table-hover">
	<tr>
		<th scope="col">Serial</th>
		<th scope="col">Name</th>
		<th scope="col">Email</th>
		<th scope="col">Skill</th>
		<th scope="col">Action</th>
	</tr>
	<?php if($read){?>
			<?php
			$i=1;
			while ($row = $read->fetch_assoc()) {
			?>
			<tr>
				<td><?php echo $i++ ?></td>
				<td><?php echo $row['name'];?></td>
				<td><?php echo $row['email'];?></td>
				<td><?php echo $row['skill'];?></td>
				<td><a href="update.php?id=<?php echo urlencode($row['id']);?>" class="btn btn-primary btn-sm" >Editar</a></td>
			</tr>
		
		<?php }?>
	<?php } 
	else{ echo '<p>No existen datos para mostrar!!</p>';}
	?>			
</table>
<div class="form-group">
	<span class="label label-primary"><a class="btn btn-primary text-white" href="create.php">Crear Nuevo Usuario</a></span>
	<span class="label label-primary" id="vol"><a class="btn btn-danger" href="index.php">Salir</a></span>
	<label for="" class="label label-primary">
				<span>
					<a href="index_excel.php" class="btn btn-success">Exportar a index_excel</a>
					<a href="index_word.php" class="btn btn-success">Exportar a index_word</a>
					<a href="index_pdf.php" class="btn btn-success">Exportar a pdf</a>
				</span>
	</label>
</div>
</div>
<?php include 'inc/footer.php';?>

